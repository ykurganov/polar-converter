#!/usr/bin/env python

# -*- coding: utf-8 -*-

#  Polar ProTrainer (pdd + hrm + gpx) to Garmin (tcx) converter
#  Copyright (C) 2015-2019 Yanis Kurganov <yanis.kurganov@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys, os, logging, json
import gpx, hrm, pdd, tcx

def process(sport_map, working_dir, remove_originals, pdd_file_name):
    print("Processing '%s'..." % pdd_file_name)
    for info in pdd.day_info(os.path.join(working_dir, pdd_file_name)):
        base_file_name = tcx.convert(
            sport_map,
            working_dir,
            info,
            hrm.exercise(os.path.join(working_dir, info.hrm_file_name)),
            gpx.track_points(os.path.join(working_dir, info.gpx_file_name))
        )
        if remove_originals:
            if len(info.gpx_file_name) > 0:
                os.rename(os.path.join(working_dir, info.gpx_file_name), os.path.join(working_dir, base_file_name + ".gpx"))
            if len(info.rr_file_name) > 0:
                os.remove(os.path.join(working_dir, info.rr_file_name))
            if len(info.hrm_file_name) > 0:
                os.remove(os.path.join(working_dir, info.hrm_file_name))
    if remove_originals:
        os.remove(os.path.join(working_dir, pdd_file_name))

def main(sport_map_file_path, working_dir, remove_originals):
    print("Polar ProTrainer to Garmin Converter")
    print("Copyright (C) Yanis Kurganov <yanis.kurganov@gmail.com>")
    print("Working directory: %s" % working_dir)
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        format="%(levelname)s: %(message)s",
    )
    with open(sport_map_file_path) as sport_map_file:
        sport_map = json.load(sport_map_file)
    for pdd_file_name in (file_name for file_name in os.listdir(working_dir) if os.path.isfile(os.path.join(working_dir, file_name)) and file_name.endswith(".pdd")):
        process(sport_map, working_dir, remove_originals, pdd_file_name)

main(sys.argv[1], sys.argv[2], "remove-originals" in sys.argv)

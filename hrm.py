#!/usr/bin/env python

# -*- coding: utf-8 -*-

#  Polar ProTrainer (pdd + hrm + gpx) to Garmin (tcx) converter
#  Copyright (C) 2015-2019 Yanis Kurganov <yanis.kurganov@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

from operator import itemgetter
from itertools import chain, takewhile
from functools import partial
from logging import warning
from helpers import grouper, string_to_time, local_to_utc, string_to_duration, parse_config

class exercise:
    __DATE_FORMAT = "%Y%m%d"
    __TIME_FORMAT = "%H:%M:%S.%f"

    __LAP_TIME = 0

    __SPEED_KM_H_MULT_10_TO_KM_H = 0.1
    __ALTITUDE_M_DIV_10_TO_M     = 10.0
    __ALTITUDE_IDENTITY          = 1.0

    __MONITOR_TYPES = {
        "1" : "Polar Sport Tester / Vantage XL",
        "2" : "Polar Vantage NV (VNV)",
        "3" : "Polar Accurex Plus",
        "4" : "Polar XTrainer Plus",
        "6" : "Polar S520",
        "7" : "Polar Coach",
        "8" : "Polar S210",
        "9" : "Polar S410",
        "10": "Polar S510",
        "11": "Polar S610 / S610i",
        "12": "Polar S710 / S710i / S720i",
        "13": "Polar S810 / S810i",
        "15": "Polar E600",
        "20": "Polar AXN500",
        "21": "Polar AXN700",
        "22": "Polar S625X / S725X",
        "23": "Polar S725",
        "33": "Polar CS400",
        "34": "Polar CS600X",
        "35": "Polar CS600",
        "36": "Polar RS400",
        "37": "Polar RS800",
        "38": "Polar RS800X",
    }

    def __init__(self, path):
        config = parse_config(path)
        self.__parse_params(config["Params"])
        self.__parse_lap_times(config["IntTimes"])
        self.__parse_hr_data(config["HRData"])

    def __init_mode(self, mode):
        self.__mode = {}
        if self.__version >= 106:
            if len(mode) < 8:
                raise ValueError("Wrong option 'SMode' in section 'Params'")
            self.__mode["Speed"] = True if int(mode[0]) else False
            self.__mode["Cadence"] = True if int(mode[1]) else False
            self.__mode["Altitude"] = True if int(mode[2]) else False
            self.__mode["Power"] = True if int(mode[3]) else False
            self.__mode["PowerLRB"] = True if int(mode[4]) else False
            self.__mode["PowerPI"] = True if int(mode[5]) else False
            self.__mode["CC"] = True if int(mode[6]) else False
            self.__mode["US"] = True if int(mode[7]) else False
            if self.__version >= 107:
                if len(mode) < 9:
                    raise ValueError("Wrong option 'SMode' in section 'Params'")
                self.__mode["AirPressure"] = True if int(mode[8]) else False
        else:
            if len(mode) < 3:
                raise ValueError("Wrong option 'Mode' in section 'Params'")
            self.__mode["Cadence"] = True if 0 == int(mode[0]) else False
            self.__mode["Altitude"] = True if 1 == int(mode[0]) else False
            self.__mode["CC"] = True if int(mode[1]) else False
            self.__mode["US"] = True if int(mode[2]) else False
        if self.__mode["US"]:
            raise NotImplementedError("US unit system")

    def __parse_params(self, section):
        params = {key.strip(): value.strip() for (key, value) in (option.split("=") for option in section if "=" in option)}
        self.__version = int(params["Version"])
        if self.__version < 102 or self.__version > 107:
            raise NotImplementedError("Unsupported version")
        self.__init_mode(params["SMode" if self.__version >= 106 else "Mode"])
        self.monitor = self.__MONITOR_TYPES[params["Monitor"]] if params["Monitor"] in self.__MONITOR_TYPES else "Unknown"
        self.local_time = string_to_time(params["Date"] + params["StartTime"], self.__DATE_FORMAT + self.__TIME_FORMAT)
        self.utc_time = local_to_utc(self.local_time)
        self.__duration = string_to_duration(params["Length"], self.__TIME_FORMAT)
        self.interval = int(params["Interval"])
        if self.interval == 204:
            raise NotImplementedError("Intermediate times only")
        if self.interval == 238:
            raise NotImplementedError("R-R data")
        if self.interval > 60:
            warning("Recording interval is more than 60 seconds")

    def __parse_lap_times(self, section):
        section = list(filter(None, section))
        group_size = 28 if self.__version >= 106 else 16
        if 0 != len(section) % group_size:
            raise ValueError("Wrong section 'IntTimes'")
        self.lap_times = sorted(
            list(
                filter(
                    None,
                    map(
                        partial(string_to_duration, format=self.__TIME_FORMAT),
                        map(
                            itemgetter(self.__LAP_TIME),
                            grouper(section, group_size)
                        )
                    )
                )
            )
        )
        if not self.lap_times or self.lap_times[-1] < self.__duration:
            self.lap_times.append(self.__duration)
        elif self.lap_times[-1] > self.__duration:
            self.lap_times = list(chain(takewhile(lambda d: d < self.__duration, self.lap_times), [self.__duration]))

    def __init_indices(self):
        self.indices = {"HR": 0}
        self.__hr_group_size = 1
        if self.__version >= 106:
            if self.__mode["Speed"]:
                self.indices["Speed"] = self.__hr_group_size
                self.__hr_group_size += 1
            if self.__mode["Cadence"]:
                self.indices["Cadence"] = self.__hr_group_size
                self.__hr_group_size += 1
            if self.__mode["Altitude"]:
                self.indices["Altitude"] = self.__hr_group_size
                self.__hr_group_size += 1
            if self.__mode["Power"]:
                self.__hr_group_size += 1
            if self.__mode["PowerLRB"] or self.__mode["PowerPI"]:
                self.__hr_group_size += 1
            if self.__version >= 107:
                if self.__mode["AirPressure"]:
                    self.__hr_group_size += 1
        else:
            self.indices["Speed"] = self.__hr_group_size
            self.__hr_group_size += 1
            if self.__mode["Cadence"]:
                self.indices["Cadence"] = self.__hr_group_size
                self.__hr_group_size += 1
            elif self.__mode["Altitude"]:
                self.indices["Altitude"] = self.__hr_group_size
                self.__hr_group_size += 1

    def __fix_bundles(self, bundle):
        result = []
        for name, index in sorted(self.indices.items(), key=itemgetter(1)):
            if name == "HR" or name == "Cadence":
                if int(bundle[index]) < 0:
                    raise ValueError("Negative '%s'" % name)
                result.append(int(bundle[index]))
            elif name == "Speed":
                result.append(float(bundle[index]) * self.__SPEED_KM_H_MULT_10_TO_KM_H)
            elif name == "Altitude":
                result.append(float(bundle[index]) * (self.__ALTITUDE_IDENTITY if self.__version >= 105 else self.__ALTITUDE_M_DIV_10_TO_M))
        return tuple(result)

    def __parse_hr_data(self, section):
        self.__init_indices()
        section = list(filter(None, section))
        if 0 != len(section) % self.__hr_group_size:
            raise ValueError("Wrong section 'HRData'")
        self.bundles = list(map(self.__fix_bundles, grouper(section, self.__hr_group_size)))

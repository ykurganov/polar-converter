#!/usr/bin/env python

# -*- coding: utf-8 -*-

#  Polar ProTrainer (pdd + hrm + gpx) to Garmin (tcx) converter
#  Copyright (C) 2015-2019 Yanis Kurganov <yanis.kurganov@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
from datetime import timedelta
from xml.dom import minidom
from logging import info, warning
from helpers import annotated_last, set_text, create_element, create_text_element

__DATE_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
__SPEED_KM_H_TO_M_S = 0.277777777778

def convert(sport_map, working_dir, info, exercise, track_points):
    base_file_name = exercise.local_time.strftime("%Y-%m-%d_%H-%M-%S")
    tcx_file_name = base_file_name + ".tcx"
    print("Creating '%s'..." % tcx_file_name)

    doc = minidom.getDOMImplementation().createDocument(None, "TrainingCenterDatabase", None)
    doc.documentElement.setAttribute("xsi:schemaLocation", "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd")
    doc.documentElement.setAttribute("xmlns:ns5", "http://www.garmin.com/xmlschemas/ActivityGoals/v1")
    doc.documentElement.setAttribute("xmlns:ns3", "http://www.garmin.com/xmlschemas/ActivityExtension/v2")
    doc.documentElement.setAttribute("xmlns:ns2", "http://www.garmin.com/xmlschemas/UserProfile/v2")
    doc.documentElement.setAttribute("xmlns", "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")
    doc.documentElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
    doc.documentElement.setAttribute("xmlns:ns4", "http://www.garmin.com/xmlschemas/ProfileExtension/v1")

    activity_element = create_element(doc, create_element(doc, doc.documentElement, "Activities"), "Activity")
    activity_element.setAttribute("Sport", sport_map.get(info.sport, "Other"))
    author_element = create_element(doc, doc.documentElement, "Author")
    author_element.setAttribute("xsi:type", "Application_t")
    create_text_element(doc, author_element, "Name", "Polar ProTrainer to Garmin Converter (C) Yanis Kurganov (yanis.kurganov at gmail.com)")
    create_text_element(doc, activity_element, "Id", exercise.utc_time.strftime(__DATE_TIME_FORMAT))

    duration_total = timedelta()
    distance_total = 0.0
    energy_elements = []
    warn_missing_energy = False
    warn_missing_hr = False
    warn_empty_lap = False

    for lap_time in exercise.lap_times:
        duration_lap = timedelta()
        distance_lap = 0.0
        hr_count = 0
        hr_sum = 0
        hr_max = 0
        speed_count = 0
        speed_max = 0.0
        speed_sum = 0.0
        empty_lap = True

        lap_element = create_element(doc, activity_element, "Lap")
        lap_element.setAttribute("StartTime", exercise.utc_time.strftime(__DATE_TIME_FORMAT))
        duration_element = create_element(doc, lap_element, "TotalTimeSeconds")
        distance_element = create_element(doc, lap_element, "DistanceMeters")
        speed_max_element = create_element(doc, lap_element, "MaximumSpeed")
        energy_element = create_element(doc, lap_element, "Calories")
        hr_avg_element = create_element(doc, lap_element, "AverageHeartRateBpm")
        hr_max_element = create_element(doc, lap_element, "MaximumHeartRateBpm")
        create_text_element(doc, lap_element, "Intensity", "Active")
        create_text_element(doc, lap_element, "TriggerMethod", "Manual")
        speed_avg_element = create_element(doc, create_element(doc, create_element(doc, lap_element, "Extensions"), "ns3:LX"), "ns3:AvgSpeed")
        track_element = create_element(doc, lap_element, "Track")

        while duration_total < lap_time and exercise.bundles:
            duration_lap += timedelta(seconds=exercise.interval)
            duration_total += timedelta(seconds=exercise.interval)
            bundle = exercise.bundles.pop(0)
            speed = (bundle[exercise.indices["Speed"]] if "Speed" in exercise.indices else 0.0) * __SPEED_KM_H_TO_M_S
            meters = speed * exercise.interval
            distance_lap += meters
            distance_total += meters
            hr = bundle[exercise.indices["HR"]]
            hr_count += 1
            hr_sum += hr
            hr_max = max(hr_max, hr)
            speed_count += 1
            speed_sum += speed
            speed_max = max(speed_max, speed)
            empty_lap = False

            track_point_element = create_element(doc, track_element, "Trackpoint")
            create_text_element(doc, track_point_element, "Time", exercise.utc_time.strftime(__DATE_TIME_FORMAT))
            create_text_element(doc, track_point_element, "DistanceMeters", distance_total)

            if "Altitude" in exercise.indices:
                create_text_element(doc, track_point_element, "AltitudeMeters", bundle[exercise.indices["Altitude"]])

            if "Cadence" in exercise.indices:
                create_text_element(doc, track_point_element, "Cadence", bundle[exercise.indices["Cadence"]])

            if hr != 0:
                create_text_element(doc, create_element(doc, track_point_element, "HeartRateBpm"), "Value", hr)

            if track_points:
                if track_points[0].utc_time <= exercise.utc_time:
                    track_point = track_points.pop(0)
                    position_element = create_element(doc, track_point_element, "Position")
                    create_text_element(doc, position_element, "LatitudeDegrees", track_point.latitude)
                    create_text_element(doc, position_element, "LongitudeDegrees", track_point.longitude)

            create_text_element(doc, create_element(doc, create_element(doc, track_point_element, "Extensions"), "ns3:TPX"), "ns3:Speed", speed)
            exercise.utc_time += timedelta(seconds=exercise.interval)

        if not empty_lap:
            set_text(doc, duration_element, duration_lap.total_seconds())
            set_text(doc, distance_element, distance_lap)

            set_text(doc, speed_max_element, speed_max)
            set_text(doc, speed_avg_element, speed_sum / speed_count)

            if info.energy:
                energy_elements.append(energy_element)
            else:
                warn_missing_energy = True
                lap_element.removeChild(energy_element)

            if hr_max != 0:
                create_text_element(doc, hr_max_element, "Value", hr_max)
                create_text_element(doc, hr_avg_element, "Value", round(hr_sum / hr_count))
            else:
                warn_missing_hr = True
                lap_element.removeChild(hr_max_element)
                lap_element.removeChild(hr_avg_element)
        else:
            warn_empty_lap = True
            activity_element.removeChild(lap_element)

    for energy_element, last in annotated_last(energy_elements):
        set_text(doc, energy_element, int(info.energy / len(energy_elements)) + (int(info.energy % len(energy_elements)) if last else 0))

    creator_element = create_element(doc, activity_element, "Creator")
    creator_element.setAttribute("xsi:type", "Device_t")
    create_text_element(doc, creator_element, "Name", exercise.monitor)

    if warn_missing_energy:
        warning("Missing energy")
    if warn_missing_hr:
        warning("Missing HR")
    if warn_empty_lap:
        warning("Empty lap")

    with open(os.path.join(working_dir, tcx_file_name), "wb") as file:
        file.write(doc.toxml(encoding="utf-8"))

    return base_file_name

#!/usr/bin/env python

# -*- coding: utf-8 -*-

#  Polar ProTrainer (pdd + hrm + gpx) to Garmin (tcx) converter
#  Copyright (C) 2015-2019 Yanis Kurganov <yanis.kurganov@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

import re, time
from itertools import chain, tee, zip_longest
from datetime import datetime, timedelta, timezone

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def annotated_last(iterable):
    "Returns an iterable of pairs of input item and a boolean that show if the current item is the last item in the sequence"
    MISSING = object()
    for current_item, next_item in pairwise(chain(iterable, [MISSING])):
        yield current_item, next_item is MISSING

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    "grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

def string_to_time(string, format):
    return datetime.strptime(string, format)

def string_to_duration(string, format):
    time = string_to_time(string, format)
    return timedelta(hours=time.hour, minutes=time.minute, seconds=time.second, microseconds=time.microsecond)

def time_to_string(time, format):
    return time.strftime(format)

def local_to_utc(local):
    return datetime.fromtimestamp(time.mktime(local.timetuple()), timezone.utc)

def get_element(node, name):
    childs = [i for i in node.childNodes if i.nodeType == i.ELEMENT_NODE and i.nodeName == name]
    if 1 != len(childs):
        raise SyntaxError("Multiple elements was found")
    return childs[0]

def get_text(node, name):
    childs = [i for i in get_element(node, name).childNodes if i.nodeType == i.TEXT_NODE]
    if 1 != len(childs):
        raise SyntaxError("Multiple elements was found")
    return childs[0].nodeValue

def set_text(doc, element, value):
    element.appendChild(doc.createTextNode(str(value)))
    return element

def create_element(doc, parent, name):
    return parent.appendChild(doc.createElement(name))

def create_text_element(doc, parent, name, value):
    return set_text(doc, create_element(doc, parent, name), value)

def parse_config(path):
    bundle = {}
    section = None
    for line in open(path, "r"):
        m = re.match(r"\s*\[([a-zA-Z0-9-]+)\]\s*", line)
        if m:
            section = m.group(1)
            bundle[section] = []
        else:
            if section is None:
                section = "Default"
                bundle[section] = []
            bundle[section].extend(i.strip() for i in line.split("\t"))
    return bundle

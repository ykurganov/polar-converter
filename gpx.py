#!/usr/bin/env python

# -*- coding: utf-8 -*-

#  Polar ProTrainer (pdd + hrm + gpx) to Garmin (tcx) converter
#  Copyright (C) 2015-2019 Yanis Kurganov <yanis.kurganov@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

from collections import UserList
from xml.dom import minidom
from logging import warning
from helpers import string_to_time, local_to_utc, get_text

class track_point:
    def __init__(self, trkpt):
        self.local_time = string_to_time(get_text(trkpt, "time"), "%Y-%m-%dT%H:%M:%SZ")
        self.utc_time = local_to_utc(self.local_time)
        self.latitude = float(trkpt.getAttribute("lat"))
        self.longitude = float(trkpt.getAttribute("lon"))

class track_points(UserList):
    def __init__(self, path):
        super().__init__()
        try:
            self.data.extend(track_point(trkpt) for trkpt in minidom.parse(path).getElementsByTagName("trkpt"))
        except OSError:
            warning("Empty GPS Data")

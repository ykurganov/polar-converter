#!/usr/bin/env python

# -*- coding: utf-8 -*-

#  Polar ProTrainer (pdd + hrm + gpx) to Garmin (tcx) converter
#  Copyright (C) 2015-2019 Yanis Kurganov <yanis.kurganov@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys, os
from helpers import string_to_time, time_to_string

def process(working_dir, tcx_file_names, gpx_file_name):
    print("Processing '%s'..." % gpx_file_name)
    gpx_time = string_to_time(gpx_file_name, "%Y-%m-%d_%H-%M-%S.gpx")
    for tcx_file_name in tcx_file_names:
        tcx_time = string_to_time(tcx_file_name, "%Y-%m-%d_%H-%M-%S.tcx")
        if (gpx_time.date() == tcx_time.date() and gpx_time.time() != tcx_time.time()):
            if (gpx_time.time().hour != tcx_time.time().hour and gpx_time.time().minute == tcx_time.time().minute and gpx_time.time().second == tcx_time.time().second):
                os.rename(os.path.join(working_dir, gpx_file_name), os.path.join(working_dir, time_to_string(tcx_time, "%Y-%m-%d_%H-%M-%S") + ".gpx"))

def main(working_dir):
    print("Polar ProTrainer to Garmin Converter")
    print("Copyright (C) Yanis Kurganov <yanis.kurganov@gmail.com>")
    print("Working directory: %s" % working_dir)
    tcx_file_names = [file_name for file_name in os.listdir(working_dir) if os.path.isfile(os.path.join(working_dir, file_name)) and file_name.endswith(".tcx")]
    for gpx_file_name in (file_name for file_name in os.listdir(working_dir) if os.path.isfile(os.path.join(working_dir, file_name)) and file_name.endswith(".gpx")):
        process(working_dir, tcx_file_names, gpx_file_name)

main(sys.argv[1])
